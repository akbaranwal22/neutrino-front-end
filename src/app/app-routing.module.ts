import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserLoginComponent } from './component/login/user-login/user-login.component';
import { ProductComponent } from './component/product/product.component';
import { CartComponent } from './component/cart/cart.component';
import { AddFoodProductComponent } from './component/add-food-product/add-food-product.component';

const routes: Routes = [
    { path: '', redirectTo: '/products', pathMatch: 'full' },
    { path: 'login', component: UserLoginComponent },
    { path: 'products', component: ProductComponent },
    { path: 'cart', component: CartComponent },
    { path: 'add-latest-food', component: AddFoodProductComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export const routingComponents = [
    UserLoginComponent
];
