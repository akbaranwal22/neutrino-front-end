import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-add-food-product',
  templateUrl: './add-food-product.component.html',
  styleUrls: ['./add-food-product.component.css']
})
export class AddFoodProductComponent implements OnInit {

  food: any;
  cost: any;
  describe: any;

  constructor(
    private fb: FormBuilder,
    private service: AppService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }
  
  foodProductForm: FormGroup = this.fb.group({
    foodName: ['', [Validators.required]],
    price: ['', [Validators.required]],
    description: ['', [Validators.required]],
  });

  onAddFoodProduct(): any {
    const addNewFoodDetails: any = {
      foodName: this.foodProductForm.value.foodName,
      price: this.foodProductForm.value.price,
      description:this.foodProductForm.value.description,
    };

    this.service.addFoodProduct(addNewFoodDetails).subscribe((data: any) => {
      alert("food successfully added");
      this.router.navigate(['/products']);
    })
  }

}
