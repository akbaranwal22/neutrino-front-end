import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  username: any;
  password: any;
  selectedOption: any;
  adminValidateData: any;
  hsptlDetails: any;
  login1: boolean = false;
  hide: boolean = false;

  constructor(
    private fb: FormBuilder,
    private service: AppService,
    private router: Router
  ) {}

  ngOnInit(): void {
  }

  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4)]],
  });

  signUpForm: FormGroup = this.fb.group({
    signupUsername: ['', [Validators.required]],
    signupEmail: ['', [Validators.required, Validators.email]],
    signupPassword: ['', [Validators.required, Validators.minLength(4)]],
  });

  onLogin() {
    if (!this.loginForm.valid) {
      return;
    }
  }

  onSignUpValidate() {
    if (!this.signUpForm.valid) {
      return;
    }
  }

  adminLogin() {
    this.login1 = true;
  }

  navigate(user: any, pswrd: any) {
    if (this.login1 === true) {
      this.service.getAdminUserDetails(user).subscribe((AdminData) => {
        this.adminValidateData = AdminData;        
        if (
          this.adminValidateData &&
          this.adminValidateData.password === pswrd
        ) {
          this.router.navigate(['/add-latest-food']);;
        } else {
           alert('Give Valid Credential.!');
        }
      });
    }
  }

  onSignUp() {
    const adminDetails: any = {
      username: this.signUpForm.value.signupUsername,
      email: this.signUpForm.value.signupEmail,
      password:this.signUpForm.value.signupPassword,
    };
    this.service.addNewAdmin(adminDetails).subscribe((data: any) => {      
      if(this.signUpForm.status == "VALID") {
        alert("New Admin successfully created");
        this.router.navigate(['/add-latest-food']);
      } else {
        alert("Enter valid credential !");
      }
    })
  }
}
