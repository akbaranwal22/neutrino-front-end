import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app.service';
import { CartService } from 'src/app/service/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  newFoodDetiails: any;
  foodProduct: any[] =[
    { 
      _id: "1",
      foodName: "Burger",
      price: 100,
      description: "Made of bread",
      count: 1
    },
    {
      _id: "2",
      foodName: "Aloo tikki",
      price: 159,
      description: "Patties of potato mixed with some vegetables fried",
      count: 1
    },
    {
      _id: "3",
      foodName: "Chana masala",
      price: 159,
      description: "Chickpeas of the Chana type in tomato based sauce.",
      count: 1
    }
  ]

  constructor(private service: AppService, private cartService: CartService) { }

  ngOnInit(): void {
    this.service.getNewFoodDetails().subscribe( (detail) => {      
      this.newFoodDetiails = detail;
      this.foodProduct = [...this.foodProduct, ...this.newFoodDetiails]      
    })
  }

  addtocart(item: any) {
    this.cartService.addToCart(item);
  }

}
