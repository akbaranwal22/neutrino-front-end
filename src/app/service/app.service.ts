import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private admin_url = "http://localhost:8000/admin";
  private url = "http://localhost:8000/add-food-product"
  private food_url = "http://localhost:8000/add-food-product";
  private add_new_admin_url = " http://localhost:8000/user-registration";

  constructor(private http: HttpClient, private router: Router) { }
 
  addNewAdmin(adminDetails: any) {
    return this.http.post<any>(this.add_new_admin_url, adminDetails);
  }

  getAdminUserDetails(username:any):Observable<any>{
    return this.http.get(this.admin_url+`/${username}`);
  }

  addFoodProduct(foodDetail: any) {
    return this.http.post<any>(this.url, foodDetail);
  }

  getNewFoodDetails():Observable<any> {
    return this.http.get(this.food_url);
  }
  
}
